from random import randint



name = input("What is your name?")



for guess in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)
    answer = input(f"{name} were you born in {month_number}/{year_number}?")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif guess == 5:
        print("I have other things to do, goodbye!")
    else:
         print("Drat! Lemme try again!")
